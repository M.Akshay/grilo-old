#!/bin/sh

# Add the correct paths to test executables and libraries
. common/update-test-path

CURRENT_DIR=$(pwd)
RESOURCE_DIR="${CURRENT_DIR}/TestResource"
cd
HOME_USER_DIR=$(pwd)
cd ${CURRENT_DIR}

if [ ! -d "${HOME_USER_DIR}/Pictures" ]
then
    mkdir -p "${HOME_USER_DIR}/Pictures"
fi

if [ ! -d "${HOME_USER_DIR}/Music" ]
then  
    mkdir -p "${HOME_USER_DIR}/Music"
fi

if [ ! -d "${HOME_USER_DIR}/Videos" ]
then
    mkdir -p "${HOME_USER_DIR}/Videos"
fi

if [ ! -d "${HOME_USER_DIR}/Documents" ]
then
    mkdir -p "${HOME_USER_DIR}/Documents"
fi

#test resources if needed

cp -R "${RESOURCE_DIR}/images/" "${HOME_USER_DIR}/Pictures" 
cp -R "${RESOURCE_DIR}/documents/" "${HOME_USER_DIR}/Documents" 
cp -R "${RESOURCE_DIR}/audio/" "${HOME_USER_DIR}/Music" 
cp -R "${RESOURCE_DIR}/playlists/" "${HOME_USER_DIR}/Music" 
cp -R "${RESOURCE_DIR}/videos/" "${HOME_USER_DIR}/Videos" 

browse=$(grl-launch-0.3 -k title browse grl-filesystem)

search=$(grl-launch-0.3 -k title search "generic-no-artwork" grl-filesystem)


if [ -f "${PWD}/Pictures/320px-European_Common_Frog_Rana_temporaria.jpg" ] ; then
       rm "${PWD}/Pictures/320px-European_Common_Frog_Rana_temporaria.jpg"
fi

if [ -f "a.txt" ] ; then
	rm "a.txt"
fi
touch a.txt

$(grl-launch-0.3 -k uri,url -T -S monitor grl-filesystem > a.txt &)
sleep 5

#file addition
cp "${RESOURCE_DIR}/images/320px-European_Common_Frog_Rana_temporaria.jpg" "${HOME_USER_DIR}/Pictures/"
sleep 5

#file changed
echo "modify" > "${HOME_USER_DIR}/Pictures/320px-European_Common_Frog_Rana_temporaria.jpg"
sleep 5

#file removed
rm "${HOME_USER_DIR}/Pictures/320px-European_Common_Frog_Rana_temporaria.jpg"
sleep 5

#Kill background process
process_id=$(pgrep "grl-launch")
sudo kill -9 ${process_id}

echo "||TEST RESULTS||"

testname="grilotest-browse"
if [ "${browse}" = "No results" ]
then
      echo "${testname}: fail"
      exit 1
else
      echo "${testname}: pass"
fi

testname="grilotest-search"
if [ "${search}" = "No results" ]

then
      echo "${testname}: fail"
      exit 1
else
      echo "${testname}: pass"
fi

testname="grilotest-add_notification"
if [ $(cat a.txt |grep 320px-European_Common_Frog_Rana_temporaria.jpg | grep added) ]
then
      echo "${testname}: pass"
else
      echo "${testname}: fail"
      exit 1
fi

testname="grilotest-change_notification"
if [ $(cat a.txt |grep 320px-European_Common_Frog_Rana_temporaria.jpg | grep changed | head -1) ]
then
      echo "${testname}: pass"
else
      echo "${testname}: fail"
      exit 1
fi

testname="grilotest-remove_notification"
if [ $(cat a.txt |grep 320px-European_Common_Frog_Rana_temporaria.jpg | grep removed) ]
then
      echo "${testname}: pass"
else
      echo "${testname}: fail"
      exit 1
fi 

#clean up
rm -R "${HOME_USER_DIR}/Pictures/images" 
rm -R "${HOME_USER_DIR}/Documents/documents" 
rm -R "${HOME_USER_DIR}/Music/audio" 
rm -R "${HOME_USER_DIR}/Music/playlists" 
rm -R "${HOME_USER_DIR}/Videos/videos" 
rm a.txt

